package de.deerstone.moveforcharity.authentication;

/**
 * Responsible for Authentication
 *
 * @author Dzheko Akperov
 */
public class AuthManager {

    private static final String USERNAME = "admin";

    private static final String PASSWORD = "pass";

    /**
     * Checks if sign in data are correct
     *
     * @param username
     * @param password
     * @return isAuthorised
     */
    public boolean authorize(String username, String password) {
        boolean isAuthorized = USERNAME.equals(username) && PASSWORD.equals(password);
        return isAuthorized;
    }
}
