package de.deerstone.moveforcharity.utils;

/**
 * Time Utility
 *
 * @author Dzheko Akperov
 */
public class TimeUtil {

    /**
     * Converts time to seconds
     *
     * @seconds
     */
    public static String getTimeFromSeconds(String seconds) {
        Integer minutes = Integer.valueOf(seconds) / 60;
        Integer restSeconds = Integer.valueOf(seconds) - minutes * 60;
        String restSecondsStr = "" + restSeconds;
        String restMinutesStr = "" + minutes;

        if (restSeconds < 10) {
            restSecondsStr = 0 + restSecondsStr;
        }

        if (minutes < 10) {
            restMinutesStr = 0 + restMinutesStr;
        }
        String time = restMinutesStr + ":" + restSecondsStr;
        return time;
    }

    /**
     * Converts Timestamp from time
     *
     * @param timeStamp
     */
    public static String getTimeFromTimestamp(String timeStamp) {
        String timeHour = timeStamp.charAt(8) + "" + timeStamp.charAt(9);
        String timeMinutes = timeStamp.charAt(10) + "" + timeStamp.charAt(11);
        return timeHour + "." + timeMinutes;
    }

}
