package de.deerstone.moveforcharity.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import de.deerstone.moveforcharity.interfaces.MappingEntity;

/**
 * Creates JSON Objects
 *
 * @author Dzheko Akperov
 */
public class JSONManager {

    private final static String METHOD = "METHOD";
    private final static String TABLE = "TABLE";
    private final static String VALUES = "VALUES";
    private final static String WHERE = "WHERE";

    private final static String CREATE = "CREATE";
    private final static String UPDATE = "UPDATE";
    private final static String DELETE = "DELETE";

    /**
     * Creates JSONObject
     *
     * @param hashMap
     * @return JSONObject
     */
    public static JSONObject createJSONObject(HashMap<String, String> hashMap) {
        JSONObject jsonObject = new JSONObject();
        try {
            for (String key : hashMap.keySet()) {
                jsonObject.put(key, hashMap.get(key));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }


    /**
     * Creates JSONObject for Write
     *
     * @param insertValues
     * @param insertValues
     * @return JSONObject
     */
    public static JSONObject getCREATEJsonObject(String tableName, HashMap<String, String> insertValues) {
        JSONArray insertValuesJson = new JSONArray(Arrays.asList(insertValues));
        HashMap<String, String> insertHashMap = new HashMap<>();
        insertHashMap.put(METHOD, CREATE);
        insertHashMap.put(TABLE, tableName);
        insertHashMap.put(VALUES, insertValuesJson.toString());
        return createJSONObject(insertHashMap);
    }

    /**
     * Creates JSONObject for Write
     *
     * @param updateValues
     * @param updateValues
     * @param whereClauses
     * @return JSONObject
     */
    public static JSONObject getUPDATEJsonObject(String tableName, HashMap<String, String> updateValues,
                                                 HashMap<String, String> whereClauses) {
        JSONArray updateValuesJson = new JSONArray(Arrays.asList(updateValues));
        JSONArray whereClausesJson = new JSONArray(Arrays.asList(whereClauses));
        HashMap<String, String> updateHashMap = new HashMap<>();
        updateHashMap.put(METHOD, UPDATE);
        updateHashMap.put(TABLE, tableName);
        updateHashMap.put(VALUES, updateValuesJson.toString());
        updateHashMap.put(WHERE, whereClausesJson.toString());
        return createJSONObject(updateHashMap);
    }

    /**
     * Creates Update Json with autodetection of values and columns in database
     *
     * @param objectOne
     * @param objectTwo
     * @return JSONObject
     */
    public static HashMap<String, String> createUPDATEJsonObject(Object objectOne, Object objectTwo) {
        List<Field> listFields = compareAllFields(objectOne, objectTwo);
        HashMap<String, String> updatedValuesHashMap = new HashMap<>();
        HashMap<String, String> entityColumns = getMappedColumns(objectOne);
        for (Field field : listFields) {
            String column = entityColumns.get(field.getName());
            try {
                updatedValuesHashMap.put(column, field.get(objectTwo).toString());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        }

        return updatedValuesHashMap;
    }


    /**
     * Compares two Objects
     *
     * @param objectOne
     * @param objectTwo
     * @return List of different fields
     */
    public static List<Field> compareAllFields(Object objectOne, Object objectTwo) {
        ArrayList<Field> listFields = new ArrayList<>();
        for (Field field : objectOne.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            Object valueOne = null;
            Object valueTwo = null;
            try {
                valueOne = field.get(objectOne);
                valueTwo = field.get(objectTwo);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            if (valueOne != null && !valueOne.equals(valueTwo)) {
                listFields.add(field);
            }
        }
        return listFields;
    }

    /**
     * Returns mapped columns
     *
     * @param object
     * @return hashmap
     */
    public static HashMap<String, String> getMappedColumns(Object object) {
        if (object instanceof MappingEntity) {
            return ((MappingEntity) object).getMappingColumns();
        }
        return null;
    }
}
