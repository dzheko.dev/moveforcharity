package de.deerstone.moveforcharity.utils;

import android.os.AsyncTask;

public class AsyncTaskImpl<T> extends AsyncTask<T, Void, T> {

    private Runnable preExectutor;
    private Runnable backgroundExecutor;
    private Runnable postExecutor;

    public void setPreExectutor(Runnable preExectutor) {
        this.preExectutor = preExectutor;
    }

    public void setBackgroundExecutor(Runnable backgroundExecutor) {
        this.backgroundExecutor = backgroundExecutor;
    }

    public void setPostExecutor(Runnable postExecutor) {
        this.postExecutor = postExecutor;
    }

    @Override
    protected void onPreExecute() {
        if (preExectutor != null) {
            preExectutor.run();
        }
    }

    @Override
    protected T doInBackground(T... baseEntity) {
        if (backgroundExecutor != null) {
            backgroundExecutor.run();
        }
        return null;
    }

    @Override
    protected void onPostExecute(T result) {
        if (postExecutor != null) {
            postExecutor.run();
        }
    }


}