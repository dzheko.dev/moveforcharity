package de.deerstone.moveforcharity.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

/**
 * System Helper to reduce Boiler Plate Code
 *
 * @author Dzheko Akperov
 */
public class SystemHelper {

    private final static String DRAWABLE = "drawable";
    private final static String STRING = "string";
    private final static String RAW = "raw";

    /**
     * Goes to Activity
     *
     * @param activityOne        of 1st Activity
     * @param classOfActivityTwo 2nd Activity
     */
    public static void goToActivity(Activity activityOne, Class classOfActivityTwo) {
        Intent mainpane = new Intent(activityOne, classOfActivityTwo.getClass());
        activityOne.startActivity(mainpane);
    }

    /**
     * Closes view
     *
     * @param view
     */
    public static void closeView(View view) {
        view.setVisibility(GONE);
    }

    /**
     * Makes view invisible
     *
     * @param view
     */
    public static void makeInvisible(View view) {
        view.setVisibility(INVISIBLE);
        view.setClickable(false);
    }

    /**
     * Makes view visible
     *
     * @param view
     */
    public static void makeVisible(View view) {
        view.setVisibility(VISIBLE);
        view.setClickable(true);
    }

    /**
     * Opens view
     *
     * @param view
     */
    public static void openView(View view) {
        view.setVisibility(VISIBLE);
    }

    /**
     * Changes the Image Resource
     *
     * @param imageView
     * @param resourceName
     */
    public static void changeImageViewResource(ImageView imageView, String resourceName) {
        int resourceId = 0;
        resourceId = imageView.getContext().getResources().getIdentifier(resourceName, DRAWABLE, imageView.getContext().getPackageName());
        imageView.setImageResource(resourceId);
    }

    /**
     * Gets Image from xml
     *
     * @param context
     * @param resourceName
     */
    public static int getImageResource(Context context, String resourceName) {
        int resourceId = 0;
        resourceId = context.getResources().getIdentifier(resourceName, DRAWABLE, context.getPackageName());
        return resourceId;
    }

    /**
     * Gets String Resource from xml
     *
     * @param context
     * @param resourceName
     * @return resourceId
     */
    public static String getStringResource(Context context, String resourceName) {
        int resourceId = 0;
        resourceId = context.getResources().getIdentifier(resourceName, STRING, context.getPackageName());
        return context.getResources().getString(resourceId);
    }

    /**
     * Returns Audio Resource
     *
     * @param context
     * @param resourceName
     * @return id Of Audio Resource
     */
    public static int getAudioResource(Context context, String resourceName) {
        int resourceId = 0;
        resourceId = context.getResources().getIdentifier(resourceName, RAW, context.getPackageName());
        return resourceId;
    }

    /**
     * Checks if int isBetween
     *
     * @param number
     * @param upper
     * @param lower  true if is between
     */
    public static boolean isBetween(int number, int lower, int upper) {
        return lower <= number && number <= upper;
    }

    /**
     * Converts date to String
     *
     * @param date
     * @return stringDate
     */
    public static String convertDateToString(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateTime = dateFormat.format(date);
        return dateTime;
    }

    /**
     * Converts String to Date
     *
     * @param dateString
     * @return date
     */
    public static Date convertStringToDate(String dateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * Checks whether network is Available
     *
     * @param context
     * @return boolean
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * Closed Keyboard
     *
     * @param activity
     */
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }
}

