package de.deerstone.moveforcharity.utils;

/**
 * Contains Keys for Extra
 *
 * @author Dzheko Akperov
 */
public class IntentExtra {

    public static final String RACE_NAME = "RACE_NAME";
    public static final String RACE_ID = "RACE_ID";
}
