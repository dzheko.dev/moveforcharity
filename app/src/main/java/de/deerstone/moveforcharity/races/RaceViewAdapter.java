package de.deerstone.moveforcharity.races;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import de.deerstone.moveforcharity.R;
import de.deerstone.moveforcharity.framework.gridview.GridViewAdapter;

/**
 * Race View Adapter
 */
public class RaceViewAdapter extends GridViewAdapter {

    private Context context;
    private LayoutInflater inflater;

    public RaceViewAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View v = inflater.inflate(R.layout.choose_race_listview_item, viewGroup, false);
        TextView twRaceName = v.findViewById(R.id.tw_race_name);
        TextView twRaceDate = v.findViewById(R.id.tw_race_date);

        List<RaceViewItem> raceViewItems = getGridViewItems();
        RaceViewItem raceViewItem = raceViewItems.get(i);
        twRaceName.setText(raceViewItem.getRaceName());
        twRaceDate.setText(raceViewItem.getRaceDate() + ", " + raceViewItem.getRacePlace());


        return v;
    }


}
