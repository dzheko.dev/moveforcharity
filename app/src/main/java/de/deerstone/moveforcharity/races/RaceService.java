package de.deerstone.moveforcharity.races;

import org.json.JSONArray;

import java.util.HashMap;

import de.deerstone.moveforcharity.restapi.RestApiManager;

/**
 * Helps to get Races
 *
 * @author Dzheko Akperov
 */
public class RaceService {

    private static final String REST_URL = "http://jsonparser.atwebpages.com/races.php";
    private static final String JSON_ARRAY_NAME = "RACE";

    private RestApiManager restApiManager = new RestApiManager();

    /**
     * Finds all races
     */
    public JSONArray findAllRaces() {
        return findRacesByParams(new HashMap<>());
    }

    /**
     * Finds Races by Params
     *
     * @param urlParams
     * @return JSONArray
     */
    public JSONArray findRacesByParams(HashMap<String, String> urlParams) {
        JSONArray jsonArray = null;
        try {
            jsonArray = restApiManager.getJSONfromURLwithParams(REST_URL, JSON_ARRAY_NAME, urlParams);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonArray;
    }
}
