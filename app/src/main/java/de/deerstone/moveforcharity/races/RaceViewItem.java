package de.deerstone.moveforcharity.races;

import java.util.HashMap;

import de.deerstone.moveforcharity.framework.gridview.GridViewItem;

/**
 * Race View Item
 *
 * @author Dzheko Akperov
 */
public class RaceViewItem extends GridViewItem {

    private String raceName;
    private String raceDate;
    private String racePlace;

    public RaceViewItem(Integer id, String raceName, String raceDate, String racePlace) {
        super(id);
        this.raceName = raceName;
        this.raceDate = raceDate;
        this.racePlace = racePlace;
    }

    public String getRaceName() {
        return raceName;
    }

    public void setRaceName(String raceName) {
        this.raceName = raceName;
    }

    public String getRaceDate() {
        return raceDate;
    }

    public void setRaceDate(String raceDate) {
        this.raceDate = raceDate;
    }

    public String getRacePlace() {
        return racePlace;
    }

    public void setRacePlace(String racePlace) {
        this.racePlace = racePlace;
    }

    @Override
    public HashMap<String, String> getMappingColumns() {
        return null;
    }
}
