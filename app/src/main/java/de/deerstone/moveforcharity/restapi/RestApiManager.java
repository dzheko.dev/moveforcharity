package de.deerstone.moveforcharity.restapi;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import de.deerstone.moveforcharity.utils.AsyncTaskImpl;

/**
 * Rest Api Helper
 *
 * @author Dzheko Akperov
 */
public class RestApiManager {

    /**
     * gets JSON from URL
     *
     * @param urlAddress
     */
    public JSONArray getJSONfromURL(String urlAddress, String tableName) throws Exception {
        URL url = new URL(urlAddress);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(false);
        conn.setDoInput(true);
        conn.setUseCaches(false);
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

        StringBuffer response = new StringBuffer();
        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        String responseJSON = response.toString();
        JSONObject jsonObject = new JSONObject(responseJSON);
        JSONArray jsonArray = jsonObject.getJSONArray(tableName.toLowerCase());

        return jsonArray;
    }

    /**
     * Sends JSON to URL
     *
     * @param jsonObject
     * @param urlAddress
     */
    public void sendJSONtoURL(String urlAddress, JSONObject jsonObject) {

        AsyncTaskImpl asyncTaskImpl = new AsyncTaskImpl();
        asyncTaskImpl.setBackgroundExecutor(() -> {
            try {
                URL url = new URL(urlAddress);
                String message = jsonObject.toString();

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setFixedLengthStreamingMode(message.getBytes().length);
                conn.connect();

                OutputStream outputStream = new BufferedOutputStream(conn.getOutputStream());
                outputStream.write(message.getBytes());
                outputStream.flush();
                outputStream.close();

                InputStream inputStream = conn.getInputStream();
                inputStream.close();
                conn.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        asyncTaskImpl.execute();
    }

    /**
     * Creates Url String given:
     *
     * @param urlAddress
     * @param tableName
     * @param urlParams
     */
    public JSONArray getJSONfromURLwithParams(String urlAddress, String tableName, HashMap<String, String> urlParams) throws Exception {
        String fullUrl = createUrlString(urlAddress, tableName, urlParams);
        return getJSONfromURL(fullUrl, tableName);
    }

    /**
     * Creates Url String given:
     *
     * @param urlAddress
     * @param tableName
     * @param urlParams
     */
    private String createUrlString(String urlAddress, String tableName, HashMap<String, String> urlParams) {
        String finalUrl = urlAddress + "?" + "table=" + tableName.toLowerCase();
        for (String key : urlParams.keySet()) {
            finalUrl = finalUrl + "&" + key + "=" + urlParams.get(key);
        }
        finalUrl = finalUrl.replaceAll(" ", "+");
        return finalUrl;
    }
}

