package de.deerstone.moveforcharity.interfaces;

import java.util.HashMap;

public abstract class MappingEntity {

    public abstract HashMap<String, String> getMappingColumns();
}
