package de.deerstone.moveforcharity.activity;

import android.support.v7.app.AppCompatActivity;

public abstract class AbstractActivity extends AppCompatActivity {

    /**
     * Creates view
     */
    protected abstract void createView();
}

