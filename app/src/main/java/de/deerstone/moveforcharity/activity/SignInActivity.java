package de.deerstone.moveforcharity.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.deerstone.moveforcharity.R;
import de.deerstone.moveforcharity.authentication.AuthManager;

/**
 * Sign In Activity
 *
 * @author Dzheko Akperov
 */
public class SignInActivity extends AppCompatActivity {

    @BindView(R.id.rl_btn_sign_in) RelativeLayout btnSignIn;
    @BindView(R.id.et_password) EditText etPassword;
    @BindView(R.id.et_username) EditText etUsername;

    //private AuthManager authManager = new AuthManager();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin_activity);
        ButterKnife.bind(this);

        btnSignIn.setOnClickListener(l -> {
//            SystemHelper.hideSoftKeyboard(this);
//            String username = etUsername.getText().toString();
//            String password = etPassword.getText().toString();
//            boolean isAuthorized = authManager.authorize(username, password);
//            Toast.makeText(this, isAuthorized + "", Toast.LENGTH_SHORT).show();
            Intent mainpane = new Intent(SignInActivity.this, ChooseRaceActivity.class);
            startActivity(mainpane);

        });
    }


}
