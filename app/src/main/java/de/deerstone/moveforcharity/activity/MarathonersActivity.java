package de.deerstone.moveforcharity.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.deerstone.moveforcharity.R;
import de.deerstone.moveforcharity.marathoners.MarathonerService;
import de.deerstone.moveforcharity.marathoners.MarathonerType;
import de.deerstone.moveforcharity.marathoners.MarathonerViewAdapter;
import de.deerstone.moveforcharity.marathoners.MarathonerViewItem;
import de.deerstone.moveforcharity.restapi.RestApiManager;
import de.deerstone.moveforcharity.utils.AsyncTaskImpl;
import de.deerstone.moveforcharity.utils.JSONManager;
import de.deerstone.moveforcharity.utils.SystemHelper;

import static de.deerstone.moveforcharity.marathoners.MarathonerType.BIKE;
import static de.deerstone.moveforcharity.marathoners.MarathonerType.CANOE;
import static de.deerstone.moveforcharity.marathoners.MarathonerType.RUNNER;
import static de.deerstone.moveforcharity.utils.IntentExtra.RACE_ID;
import static de.deerstone.moveforcharity.utils.IntentExtra.RACE_NAME;
import static de.deerstone.moveforcharity.utils.JSONManager.createUPDATEJsonObject;
import static de.deerstone.moveforcharity.utils.SystemHelper.changeImageViewResource;
import static de.deerstone.moveforcharity.utils.TimeUtil.getTimeFromSeconds;
import static de.deerstone.moveforcharity.utils.TimeUtil.getTimeFromTimestamp;

/**
 * Marathoners Activity with Buttons
 *
 * @author Dzheko Akperov
 */
public class MarathonersActivity extends AbstractActivity {

    @BindView(R.id.gridview_btns) GridView gridView;
    @BindView(R.id.rl_outside) RelativeLayout rlOutside;
    @BindView(R.id.rl_inside) RelativeLayout rlInside;
    @BindView(R.id.rl_popup) RelativeLayout rlDialog;

    @BindView(R.id.tw_marahoner_name) TextView twMarathonerName;
    @BindView(R.id.tw_marahone_name) TextView twMarathoneName;
    @BindView(R.id.tw_rounds_number) TextView twRoundsNumber;
    @BindView(R.id.tw_last_change) TextView twLastChange;
    @BindView(R.id.tw_last_lap_time) TextView twLastLapTime;
    @BindView(R.id.iw_marathoner_type) ImageView iwMarathonerType;
    @BindView(R.id.iw_refresh) ImageView iwRefreshBtn;
    @BindView(R.id.iw_close) ImageView iwClose;
    @BindView(R.id.progress_bar) ProgressBar progressBar;

    @BindView(R.id.rl_plus) RelativeLayout rlPlus;
    @BindView(R.id.rl_minus) RelativeLayout rlMinus;

    private ArrayList<MarathonerViewItem> marathonerViewItems;
    private MarathonerViewAdapter marathonerViewAdapter;
    private MarathonerViewItem updatedVersionMarathoner;
    private MarathonerViewItem currentVersionMarathoner;

    private static final String JSON_PROPERTY_1 = "ID";
    private static final String JSON_PROPERTY_2 = "FULLNAME";
    private static final String JSON_PROPERTY_3 = "NUMBER_OF_LAPS";
    private static final String JSON_PROPERTY_4 = "PARTICIPANT_TYPE";
    private static final String JSON_PROPERTY_5 = "LAST_ROUND_TIME";
    private static final String JSON_PROPERTY_6 = "LAST_CHANGE_TIMESTAMP";
    private static final String TABLE = "MARATHONER";

    private static final String PARAM_1 = "RACE_ID";
    private static final String URL_SEND = "http://jsonparser.atwebpages.com/races.php";


    private JSONArray jsonArray = null;
    private RestApiManager restApiManager = new RestApiManager();
    private boolean suppressListener = false;
    private Integer raceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.marathoners_activity);
        ButterKnife.bind(this);
        createView();
    }

    @Override
    protected void createView() {
        String raceName = getIntent().getStringExtra(RACE_NAME);
        twMarathoneName.setText(raceName);

        marathonerViewAdapter = new MarathonerViewAdapter(this);
        setRaceId(getIntent().getIntExtra(RACE_ID, 0));
        loadAndFillData();
        gridView.setAdapter(marathonerViewAdapter);

        iwRefreshBtn.setOnClickListener(l -> {
            loadAndFillData();
        });

        gridView.setOnItemClickListener((adapterView, view, i, l) -> {
            suppressListener = true;
            openDialog(i);
        });

        rlOutside.setOnClickListener(l -> {
            suppressListener = false;
            closeDialog();
        });

        rlPlus.setOnClickListener(l -> {
            updatedVersionMarathoner.setNumberOfLaps(updatedVersionMarathoner.getNumberOfLaps() + 1);
            fillPopDialog(updatedVersionMarathoner);
        });

        rlMinus.setOnClickListener(l -> {
            if (updatedVersionMarathoner.getNumberOfLaps() - 1 >= 0) {
                updatedVersionMarathoner.setNumberOfLaps(updatedVersionMarathoner.getNumberOfLaps() - 1);
                fillPopDialog(updatedVersionMarathoner);
            }
        });

        iwClose.setOnClickListener(l -> {
            rlOutside.performClick();
        });
    }

    /**
     * Fills the data to UI
     *
     * @param jsonArray
     */
    private void fillDataToUI(JSONArray jsonArray) {
        try {
            marathonerViewItems = convertJSONArrayToVOs(jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        marathonerViewAdapter.setGridViewItems(marathonerViewItems);
        marathonerViewAdapter.notifyDataSetChanged();
    }


    /**
     * Fills Data
     *
     * @param jsonArray
     */
    private ArrayList<MarathonerViewItem> convertJSONArrayToVOs(JSONArray jsonArray) throws JSONException {
        ArrayList<MarathonerViewItem> marathonerViewItems = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            Integer id = jsonArray.getJSONObject(i).getInt(JSON_PROPERTY_1);
            String name = jsonArray.getJSONObject(i).getString(JSON_PROPERTY_2);
            String numberOfLapsStr = jsonArray.getJSONObject(i).getString(JSON_PROPERTY_3);
            Integer numberOfLaps = Integer.valueOf(numberOfLapsStr);
            String type = jsonArray.getJSONObject(i).getString(JSON_PROPERTY_4);
            String lastRoundTime = jsonArray.getJSONObject(i).getString(JSON_PROPERTY_5);
            String lastChangeTimestamp = jsonArray.getJSONObject(i).getString(JSON_PROPERTY_6);

            MarathonerType marathonerType;
            if (BIKE.getTypeString().toLowerCase().equals(type.toLowerCase())) {
                marathonerType = BIKE;
            } else if (CANOE.getTypeString().toLowerCase().equals(type.toLowerCase())) {
                marathonerType = CANOE;
            } else {
                marathonerType = RUNNER;
            }
            Integer index = i + 1;
            MarathonerViewItem marathonerViewItem = new MarathonerViewItem(index, id, name, marathonerType, numberOfLaps, lastRoundTime, lastChangeTimestamp);
            marathonerViewItems.add(marathonerViewItem);
        }
        return marathonerViewItems;
    }

    /**
     * Loads and Fills data
     */
    protected void loadAndFillData() {
        Integer raceId = getRaceId();
        AsyncTaskImpl<String> asyncTaskImpl = new AsyncTaskImpl<>();

        asyncTaskImpl.setPreExectutor(() -> {
            progressBar.setVisibility(View.VISIBLE);
        });

        asyncTaskImpl.setBackgroundExecutor(() -> {
            MarathonerService marathonerService = new MarathonerService();
            HashMap<String, String> params = new HashMap<>();
            params.put(PARAM_1, String.valueOf(raceId));
            jsonArray = marathonerService.findMarathonersByParams(params);
        });

        asyncTaskImpl.setPostExecutor(() -> {
            fillDataToUI(jsonArray);
            progressBar.setVisibility(View.INVISIBLE);
        });

        asyncTaskImpl.execute();
    }

    /**
     * Opens Dialog of Marathoner
     *
     * @param marathonerIndex
     */
    protected void openDialog(Integer marathonerIndex) {
        SystemHelper.makeVisible(rlDialog);
        currentVersionMarathoner = marathonerViewItems.get(marathonerIndex);
        updatedVersionMarathoner = getMarathonerViewItemCopy(currentVersionMarathoner);
        fillPopDialog(currentVersionMarathoner);
    }

    /**
     * Closes Dialog of Marathoner
     */
    protected void closeDialog() {
        SystemHelper.makeInvisible(rlDialog);
        JSONObject jsonObject = getUpdateJsonObject(currentVersionMarathoner, updatedVersionMarathoner);
        restApiManager.sendJSONtoURL(URL_SEND, jsonObject);
//        if (anyChangesDone(currentVersionMarathoner, updatedVersionMarathoner)) {
//
//            currentVersionMarathoner = fillMarathonerViewItem(currentVersionMarathoner, updatedVersionMarathoner);
//        }
    }

    @Override
    public void onBackPressed() {
        if (suppressListener) {
            rlOutside.performClick();
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Fills Pop Dialog
     *
     * @param marathonerViewItem
     */
    protected void fillPopDialog(MarathonerViewItem marathonerViewItem) {
        String marathonerName = "#" + (marathonerViewItem.getIndex() + 1) + " | " + marathonerViewItem.getName();
        twMarathonerName.setText(marathonerName);
        String numberOfLaps = "#Runde: " + marathonerViewItem.getNumberOfLaps();
        twRoundsNumber.setText(numberOfLaps);
        String lastChange = "Letzte Änderung: " + getTimeFromTimestamp(marathonerViewItem.getLastChange());
        twLastChange.setText(lastChange);
        String lastRoundTime = "Letzte Runde: " + getTimeFromSeconds(marathonerViewItem.getLastRound());
        twLastLapTime.setText(lastRoundTime);
        MarathonerType marathonerType = marathonerViewItem.getMarathonerType();
        changeImageViewResource(iwMarathonerType, marathonerType.getDrawableId());
    }

    /**
     * Gets Copy Version of Marathoner
     *
     * @param marathonerViewItemToBeCopied
     * @return marathonerViewItem
     */
    protected MarathonerViewItem getMarathonerViewItemCopy(MarathonerViewItem marathonerViewItemToBeCopied) {
        MarathonerViewItem marathonerViewItem = new MarathonerViewItem(marathonerViewItemToBeCopied.getId());
        fillMarathonerViewItem(marathonerViewItem, marathonerViewItemToBeCopied);
        return marathonerViewItem;
    }

    /**
     * Fills Marathoner View Item
     *
     * @param marathonerViewItem
     * @param marathonerViewItemToBeCopied
     * @return marathonerViewItem
     */
    protected MarathonerViewItem fillMarathonerViewItem(MarathonerViewItem marathonerViewItem, MarathonerViewItem marathonerViewItemToBeCopied) {
        marathonerViewItem.setNumberOfLaps(marathonerViewItemToBeCopied.getNumberOfLaps());
        marathonerViewItem.setLastRound(marathonerViewItemToBeCopied.getLastRound());
        marathonerViewItem.setLastChange(marathonerViewItemToBeCopied.getLastChange());
        marathonerViewItem.setName(marathonerViewItemToBeCopied.getName());
        marathonerViewItem.setMarathonerType(marathonerViewItemToBeCopied.getMarathonerType());
        marathonerViewItem.setIndex(marathonerViewItemToBeCopied.getIndex());
        return marathonerViewItem;
    }

    /**
     * Any changes done?
     *
     * @param currentVersionMarathoner
     * @param updatedVersionMarathoner
     * @return true if any done
     */
    protected boolean anyChangesDone(MarathonerViewItem currentVersionMarathoner, MarathonerViewItem updatedVersionMarathoner) {
        boolean anyChangesDone = !currentVersionMarathoner.getLastChange().equals(updatedVersionMarathoner.getLastChange())
                || !currentVersionMarathoner.getLastRound().equals(updatedVersionMarathoner.getLastRound())
                || !currentVersionMarathoner.getNumberOfLaps().equals(updatedVersionMarathoner.getNumberOfLaps());
        return anyChangesDone;
    }

    /**
     * Converts marathoner to JSONObject
     *
     * @param marathonerViewItem
     * @return JSONObject
     */
    protected JSONObject convertMarathonerToJSONObject(MarathonerViewItem marathonerViewItem) {
        HashMap<String, String> marathonerHashMap = new HashMap<>();
        marathonerHashMap.put(JSON_PROPERTY_1, String.valueOf(marathonerViewItem.getId()));
        marathonerHashMap.put(JSON_PROPERTY_2, marathonerViewItem.getName());
        marathonerHashMap.put(JSON_PROPERTY_3, String.valueOf(marathonerViewItem.getNumberOfLaps()));
        marathonerHashMap.put(JSON_PROPERTY_4, marathonerViewItem.getMarathonerType().getTypeString());
        marathonerHashMap.put(JSON_PROPERTY_5, String.valueOf(marathonerViewItem.getLastRound()));
        marathonerHashMap.put(JSON_PROPERTY_6, String.valueOf(marathonerViewItem.getLastChange()));
        return JSONManager.createJSONObject(marathonerHashMap);
    }

    public Integer getRaceId() {
        return raceId;
    }

    public void setRaceId(Integer raceId) {
        this.raceId = raceId;
    }

    /**
     * Returns JSONObject
     *
     * @return jsonObject
     */
    public JSONObject getUpdateJsonObject(MarathonerViewItem marathonerViewItemOld,
                                          MarathonerViewItem marathonerViewItemNew) {
        HashMap<String, String> updateValues = createUPDATEJsonObject(marathonerViewItemOld, marathonerViewItemNew);
        HashMap<String, String> whereClauses = new HashMap<>();
        whereClauses.put(JSON_PROPERTY_1, String.valueOf(marathonerViewItemOld.getId()));
        return JSONManager.getUPDATEJsonObject(TABLE, updateValues, whereClauses);
    }


}
