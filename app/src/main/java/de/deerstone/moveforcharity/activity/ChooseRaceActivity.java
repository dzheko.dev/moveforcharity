package de.deerstone.moveforcharity.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.deerstone.moveforcharity.R;
import de.deerstone.moveforcharity.races.RaceService;
import de.deerstone.moveforcharity.races.RaceViewAdapter;
import de.deerstone.moveforcharity.races.RaceViewItem;
import de.deerstone.moveforcharity.restapi.RestApiManager;
import de.deerstone.moveforcharity.utils.AsyncTaskImpl;
import de.deerstone.moveforcharity.utils.JSONManager;

import static de.deerstone.moveforcharity.utils.IntentExtra.RACE_ID;
import static de.deerstone.moveforcharity.utils.IntentExtra.RACE_NAME;

/**
 * Choose Race Activity
 *
 * @author Dzheko Akperov
 */
public class ChooseRaceActivity extends AbstractActivity {

    @BindView(R.id.iw_refresh) ImageView iwRefresh;
    @BindView(R.id.gridview_btns) GridView gridView;
    @BindView(R.id.progress_bar) ProgressBar progressBar;

    private JSONArray jsonArray;
    private AsyncTaskImpl<String> asyncTaskImpl;
    private RaceViewAdapter raceViewAdapter;
    private ArrayList<RaceViewItem> raceViewItems = new ArrayList<>();

    private static final String JSON_PROPERTY_1 = "ID";
    private static final String JSON_PROPERTY_2 = "TITLE";
    private static final String JSON_PROPERTY_3 = "RACE_DATE";
    private static final String JSON_PROPERTY_4 = "PLACE";

    private static final String POST_URL = "http://jsonparser.atwebpages.com/races.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_race_activity);
        ButterKnife.bind(this);
        createView();
    }


    @Override
    protected void createView() {
        raceViewAdapter = new RaceViewAdapter(this);
        loadAndFillData();
        gridView.setAdapter(raceViewAdapter);


        gridView.setOnItemClickListener((adapterView, view, i, l) -> {
            Intent mainpane = new Intent(ChooseRaceActivity.this, MarathonersActivity.class);
            String raceName = raceViewItems.get(i).getRaceName();
            mainpane.putExtra(RACE_NAME, raceName);
            mainpane.putExtra(RACE_ID, i + 1);
            startActivity(mainpane);
        });


        iwRefresh.setOnClickListener(l -> {
            RestApiManager restApiManager = new RestApiManager();
            restApiManager.sendJSONtoURL(POST_URL, getJsonObject());
            //loadAndFillData();
        });
    }

    /**
     * Fills the data to UI
     *
     * @param jsonArray
     */
    private void fillDataToUI(JSONArray jsonArray) {
        try {
            raceViewItems = convertJSONArrayToVOs(jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        raceViewAdapter.setGridViewItems(raceViewItems);
        raceViewAdapter.notifyDataSetChanged();
    }

    /**
     * Fills Data
     *
     * @param jsonArray
     */
    private ArrayList<RaceViewItem> convertJSONArrayToVOs(JSONArray jsonArray) throws JSONException {
        ArrayList<RaceViewItem> raceViewItems = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            Integer raceId = jsonArray.getJSONObject(i).getInt(JSON_PROPERTY_1);
            String raceName = jsonArray.getJSONObject(i).getString(JSON_PROPERTY_2);
            String raceDate = jsonArray.getJSONObject(i).getString(JSON_PROPERTY_3);
            String racePlace = jsonArray.getJSONObject(i).getString(JSON_PROPERTY_4);
            RaceViewItem raceViewItem = new RaceViewItem(raceId, raceName, raceDate, racePlace);
            raceViewItems.add(raceViewItem);
        }
        return raceViewItems;
    }

    /**
     * Loads and Fills data
     */
    private void loadAndFillData() {
        asyncTaskImpl = new AsyncTaskImpl<>();

        asyncTaskImpl.setPreExectutor(() -> {
            progressBar.setVisibility(View.VISIBLE);
        });

        asyncTaskImpl.setBackgroundExecutor(() -> {
            RaceService raceService = new RaceService();
            jsonArray = raceService.findAllRaces();
        });

        asyncTaskImpl.setPostExecutor(() -> {
            fillDataToUI(jsonArray);
            progressBar.setVisibility(View.INVISIBLE);
        });

        asyncTaskImpl.execute();
    }

    /**
     * Returns JSONObject
     *
     * @return jsonObject
     */
    public JSONObject getJsonObject() {
        HashMap<String, String> updateValues = new HashMap<>();
        updateValues.put("T_COLUMN", "Ramiz");

        HashMap<String, String> whereClauses = new HashMap<>();
        whereClauses.put("T_COLUMN", "Richtig");
        //whereClauses.put("MUSIC", "RAMUSIC");
        //updateValues.put("MUSIC", "RAMUOIC");

        return JSONManager.getUPDATEJsonObject("TEST", updateValues, whereClauses);
    }


}
