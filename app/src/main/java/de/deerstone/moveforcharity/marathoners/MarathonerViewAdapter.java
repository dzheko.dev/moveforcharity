package de.deerstone.moveforcharity.marathoners;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import de.deerstone.moveforcharity.R;
import de.deerstone.moveforcharity.framework.gridview.GridViewAdapter;

import static de.deerstone.moveforcharity.marathoners.MarathonerType.BIKE;
import static de.deerstone.moveforcharity.marathoners.MarathonerType.CANOE;
import static de.deerstone.moveforcharity.utils.SystemHelper.changeImageViewResource;

public class MarathonerViewAdapter extends GridViewAdapter {

    private Context context;
    private final static String TYPE_BIKE = "marathoner_ic_bike";
    private final static String TYPE_CANOE = "marathoner_ic_canoe";
    private final static String TYPE_RUNNER = "marathoner_ic_runner";

    public MarathonerViewAdapter(Context context) {
        this.context = context;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.marathoners_gridview_item, viewGroup, false);

        TextView twMarathonerNumber = v.findViewById(R.id.tw_marathoner_number);
        ImageView iwMarathonerType = v.findViewById(R.id.iw_marathoner_type);

        List<MarathonerViewItem> marathonerViewItems = getGridViewItems();
        MarathonerViewItem marathonerViewItem = marathonerViewItems.get(i);
        if (BIKE.equals(marathonerViewItem.getMarathonerType())) {
            changeImageViewResource(iwMarathonerType, TYPE_BIKE);
        } else if (CANOE.equals(marathonerViewItem.getMarathonerType())) {
            changeImageViewResource(iwMarathonerType, TYPE_CANOE);
        } else {
            changeImageViewResource(iwMarathonerType, TYPE_RUNNER);
        }
        twMarathonerNumber.setText(marathonerViewItem.getIndex() + "");

        return v;
    }

}
