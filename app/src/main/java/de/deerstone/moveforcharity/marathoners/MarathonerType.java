package de.deerstone.moveforcharity.marathoners;

public enum MarathonerType {

    CANOE("CANOE", "marathoner_ic_canoe"),

    RUNNER("RUNNER", "marathoner_ic_runner"),

    BIKE("BIKE", "marathoner_ic_bike");

    private String typeString;
    private String drawableId;

    MarathonerType(String typeString, String drawableId) {
        this.typeString = typeString;
        this.drawableId = drawableId;
    }

    public String getTypeString() {
        return typeString;
    }

    public String getDrawableId() {
        return drawableId;
    }
}
