package de.deerstone.moveforcharity.marathoners;

import java.util.HashMap;

import de.deerstone.moveforcharity.framework.gridview.GridViewItem;

/**
 * Marathoner View Item
 *
 * @author Dzheko Akperov
 */
public class MarathonerViewItem extends GridViewItem {

    private Integer index;
    private String name;
    private Integer numberOfLaps;
    private MarathonerType marathonerType;
    private String lastRound;
    private String lastChange;

    public static final HashMap<String, String> COLUMNS_HASHMAP = new HashMap<>();

    /**
     * @param id
     */
    public MarathonerViewItem(Integer id) {
        super(id);
    }

    public MarathonerViewItem(Integer index, Integer id, String name, MarathonerType marathonerType, Integer numberOfLaps, String lastRound, String lastChange) {
        super(id);
        this.index = index;
        this.name = name;
        this.marathonerType = marathonerType;
        this.numberOfLaps = numberOfLaps;
        this.lastRound = lastRound;
        this.lastChange = lastChange;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumberOfLaps() {
        return numberOfLaps;
    }

    public void setNumberOfLaps(Integer numberOfLaps) {
        this.numberOfLaps = numberOfLaps;
    }

    public MarathonerType getMarathonerType() {
        return marathonerType;
    }

    public void setMarathonerType(MarathonerType marathonerType) {
        this.marathonerType = marathonerType;
    }

    public String getLastRound() {
        return lastRound;
    }

    public void setLastRound(String lastRound) {
        this.lastRound = lastRound;
    }

    public String getLastChange() {
        return lastChange;
    }

    public void setLastChange(String lastChange) {
        this.lastChange = lastChange;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    @Override
    public HashMap<String, String> getMappingColumns() {
        COLUMNS_HASHMAP.put("id", "ID");
        COLUMNS_HASHMAP.put("name", "FULLNAME");
        COLUMNS_HASHMAP.put("numberOfLaps", "NUMBER_OF_LAPS");
        COLUMNS_HASHMAP.put("marathonerType", "PARTICIPANT_TYPE");
        COLUMNS_HASHMAP.put("lastRound", "LAST_ROUND_TIME");
        COLUMNS_HASHMAP.put("lastChange", "LAST_CHANGE_TIMESTAMP");
        return COLUMNS_HASHMAP;
    }
}
