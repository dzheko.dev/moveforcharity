package de.deerstone.moveforcharity.marathoners;

import org.json.JSONArray;

import java.util.HashMap;

import de.deerstone.moveforcharity.restapi.RestApiManager;

/**
 * Service for Marathoner
 *
 * @author Dzheko Akperov
 */
public class MarathonerService {

    private static final String REST_URL = "http://jsonparser.atwebpages.com/races.php";
    private static final String JSON_ARRAY_NAME = "MARATHONER";

    private RestApiManager restApiManager = new RestApiManager();

    /**
     * Finds all marathoners
     */
    public JSONArray findAllMarathoners() {
        return findMarathonersByParams(new HashMap<>());
    }

    /**
     * Finds Races by Params
     *
     * @param urlParams
     * @return JSONArray
     */
    public JSONArray findMarathonersByParams(HashMap<String, String> urlParams) {
        JSONArray jsonArray = null;
        try {
            jsonArray = restApiManager.getJSONfromURLwithParams(REST_URL, JSON_ARRAY_NAME, urlParams);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

}
