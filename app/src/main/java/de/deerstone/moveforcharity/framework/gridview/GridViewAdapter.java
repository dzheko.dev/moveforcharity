package de.deerstone.moveforcharity.framework.gridview;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Levels Grid View Adapter
 *
 * @author Dzheko Akperov
 */
public class GridViewAdapter<T extends GridViewItem> extends BaseAdapter {

    private List<T> gridViewItemList = new ArrayList<T>();

    @Override
    public int getCount() {
        int numberOfItems = getGridViewItems() != null ? getGridViewItems().size() : 0;
        return numberOfItems;
    }

    @Override
    public Object getItem(int i) {
        return getGridViewItems().get(i);
    }

    @Override
    public long getItemId(int i) {
        return getGridViewItems().get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        return null;
    }

    /**
     * fill Grid View Items
     *
     * @param gridViewItemList
     */
    public void setGridViewItems(ArrayList<T> gridViewItemList) {
        this.gridViewItemList = gridViewItemList;
    }

    /**
     * Gets GridViewItems
     */
    public List<T> getGridViewItems() {
        return gridViewItemList;
    }
}
