package de.deerstone.moveforcharity.framework.gridview;

import de.deerstone.moveforcharity.interfaces.MappingEntity;

/**
 * GridView Item
 */
public abstract class GridViewItem extends MappingEntity {

    private Integer id;


    /**
     *
     * */
    public GridViewItem(Integer id) {
        this.id = id;
    }

    /**
     * Gets id
     */
    public Integer getId() {
        return id;
    }


}
